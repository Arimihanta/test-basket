package com.kata.basket.payload;

import lombok.Data;

/**
 * @author Andriambolaharimihanta Havana
 * @created 26/05/2024
 * @project basket
 */

@Data
public class BasketContentRequest {
    private Long productId ;
    private int quantity ;
}
