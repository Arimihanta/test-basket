package com.kata.basket.service;

import com.kata.basket.dto.UserDTO;
import com.kata.basket.exception.UserNotFoundException;
import com.kata.basket.model.User;
import com.kata.basket.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * @author Andriambolaharimihanta Havana
 * @created 25/05/2024
 * @project basket
 */

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements IUserService{
    private final UserRepository userRepository ;
    @Override
    public User findById(long id) {
        User user=userRepository.findById(id).orElseThrow(()->new UserNotFoundException(String.format("The user with ID '%s' does not exist.",id))) ;
        return user ;
    }
}
