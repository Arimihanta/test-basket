package com.kata.basket.service;

import com.kata.basket.dto.ProductDTO;
import com.kata.basket.model.Product;

import java.util.List;

/**
 * @author Andriambolaharimihanta Havana
 * @created 25/05/2024
 * @project basket
 */

public interface IProductService {
    public List<ProductDTO> findAll() ;
    public Product updateProduct(Product product) ;
    ProductDTO findById(Long id) ;
}
