package com.kata.basket.service;

import com.kata.basket.dto.BasketDTO;
import com.kata.basket.dto.UserDTO;
import com.kata.basket.model.Basket;

import java.util.List;

/**
 * @author Andriambolaharimihanta Havana
 * @created 25/05/2024
 * @project basket
 */

public interface IBasketService {
    Basket create(Basket basket) ;

    BasketDTO createOrGetLastBasket(long userId) ;

    Basket update(Basket basket) ;

    List<BasketDTO> getBasketsByUserId(long userId) ;

    BasketDTO findById(long id) ;

}
