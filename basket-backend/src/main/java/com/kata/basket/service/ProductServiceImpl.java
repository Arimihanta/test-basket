package com.kata.basket.service;

import com.kata.basket.dto.ProductDTO;
import com.kata.basket.exception.ProductNotFoundException;
import com.kata.basket.model.Product;
import com.kata.basket.repository.ProductRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Andriambolaharimihanta Havana
 * @created 25/05/2024
 * @project basket
 */


@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements IProductService{
    private final ProductRepository productRepository;
    private final ModelMapper modelMapper;
    @Override
    public List<ProductDTO> findAll() {
        List<Product> products=productRepository.findAll() ;
        return products.stream().map(product -> modelMapper.map(product, ProductDTO.class)).toList();
    }

    @Transactional
    @Override
    public Product updateProduct(Product product) {
        return productRepository.save(product) ;
    }

    @Override
    public ProductDTO findById(Long id) {
        Product product = productRepository.findById(id).orElseThrow(() -> new ProductNotFoundException(String.format("The product with ID '%s' does not exist.",id)));
        return modelMapper.map(product, ProductDTO.class);
    }
}
