package com.kata.basket.service;

import com.kata.basket.dto.BasketContentDTO;
import com.kata.basket.dto.BasketDTO;
import com.kata.basket.dto.ProductDTO;
import com.kata.basket.exception.BasketContentNotFoundException;
import com.kata.basket.exception.NoStockLeftException;
import com.kata.basket.model.Basket;
import com.kata.basket.model.BasketContent;
import com.kata.basket.model.Product;
import com.kata.basket.repository.BasketContentRepository;
import com.kata.basket.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Andriambolaharimihanta Havana
 * @created 25/05/2024
 * @project basket
 */

@Service
@RequiredArgsConstructor
public class BasketContentServiceImpl implements IBasketContentService{
    private final BasketContentRepository basketContentRepository ;
    private final ProductServiceImpl productService ;
    private final BasketServiceImpl basketService ;
    private final ModelMapper modelMapper;
    @Override
    public BasketContentDTO create(long basketId, long productId, int quantity) {
        ProductDTO productDTO=productService.findById(productId);
        if(productDTO.getStock()<1 || quantity>productDTO.getStock()){
            throw new NoStockLeftException(String.format("'%s' is out of stock.",productDTO.getName())) ;
        }

        Product newProduct=modelMapper.map(productDTO, Product.class) ;
        newProduct.setStock(productDTO.getStock()-quantity);
        newProduct=productService.updateProduct(newProduct) ;

        BasketDTO basketDTO=basketService.findById(basketId) ;
        BasketContent newBasketContent=new BasketContent() ;
        newBasketContent.setBasket(modelMapper.map(basketDTO, Basket.class));
        newBasketContent.setProduct(newProduct);
        newBasketContent.setQuantity(quantity);
        newBasketContent=basketContentRepository.save(newBasketContent);
        return modelMapper.map(newBasketContent, BasketContentDTO.class) ;
    }

    @Override
    public BasketContentDTO updateQuantity(long id, int quantity) {
        BasketContent basketContent=basketContentRepository.findById(id).orElseThrow(()->new BasketContentNotFoundException(String.format("The basket content with ID '%s' does not exist.",id))) ;
        Product product=basketContent.getProduct() ;
        int currentStock=product.getStock()+basketContent.getQuantity() ;
        if(currentStock<1 || basketContent.getQuantity()>currentStock){
            throw new NoStockLeftException(String.format("'%s' is out of stock.",basketContent.getProduct().getName())) ;
        }
        product.setStock(currentStock-quantity);
        productService.updateProduct(product) ;
        basketContent.setQuantity(quantity);
        return modelMapper.map(basketContent, BasketContentDTO.class) ;
    }

    @Override
    public void delete(long id) {
        basketContentRepository.deleteById(id);
    }

    @Override
    public void deleteByBasketId(long basketId) {
        List<BasketContent> basketContents=this.getBasketContentsByBasketId(basketId) ;
        for(BasketContent basketContent:basketContents){
            Product product=basketContent.getProduct() ;
            product.setStock(product.getStock()+basketContent.getQuantity());
            productService.updateProduct(product) ;
        }
        basketContentRepository.deleteByBasketId(basketId);
    }

    @Override
    public List<BasketContent> getBasketContentsByBasketId(long basketId) {
        return basketContentRepository.getBasketContentsByBasketId(basketId) ;
    }
}
