package com.kata.basket.service;

import com.kata.basket.dto.BasketDTO;
import com.kata.basket.exception.BasketNotFoundException;
import com.kata.basket.exception.EmptyBasketException;
import com.kata.basket.model.Basket;
import com.kata.basket.model.User;
import com.kata.basket.repository.BasketRepository;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author Andriambolaharimihanta Havana
 * @created 25/05/2024
 * @project basket
 */

@Service
@RequiredArgsConstructor
public class BasketServiceImpl implements IBasketService{
    private final BasketRepository basketRepository ;
    private final ModelMapper modelMapper;
    @Transactional
    @Override
    public Basket create(Basket basket) {
        return basketRepository.save(basket);
    }

    @Override
    public BasketDTO createOrGetLastBasket(long userId) {

        List<Basket> baskets= basketRepository.getBasketsByUserId(userId) ;
        Basket basket ;
        if(baskets.isEmpty()){
            Basket newBasket=new Basket() ;
            newBasket.setUser(new User(
                    1L,"John", "Doe", "John", "+33655563733","user123"
            ));
            basket= this.create(newBasket) ;
        }else{
            basket=baskets.stream().findFirst().get() ;
        }
        return modelMapper.map(basket, BasketDTO.class);
    }

    @Transactional
    @Override
    public Basket update(Basket basket) {
        basket.setModifiedAt(LocalDateTime.now());
        return basketRepository.save(basket);
    }

    @Override
    public List<BasketDTO> getBasketsByUserId(long userId) {
        List<Basket> baskets= basketRepository.getBasketsByUserId(userId) ;
        return baskets.stream().map(basket -> modelMapper.map(basket, BasketDTO.class)).toList();
    }

    @Override
    public BasketDTO findById(long id) {
        Basket basket= basketRepository.findById(id).orElseThrow(()->new BasketNotFoundException(String.format("The basket with ID '%s' does not exist.",id))) ;
        return modelMapper.map(basket, BasketDTO.class);
    }
}
