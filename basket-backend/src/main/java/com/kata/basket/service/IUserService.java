package com.kata.basket.service;

import com.kata.basket.model.User;

/**
 * @author Andriambolaharimihanta Havana
 * @created 25/05/2024
 * @project basket
 */

public interface IUserService {
    public User findById(long id) ;
}
