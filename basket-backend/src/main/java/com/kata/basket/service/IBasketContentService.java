package com.kata.basket.service;

import com.kata.basket.dto.BasketContentDTO;
import com.kata.basket.model.BasketContent;

import java.util.List;

/**
 * @author Andriambolaharimihanta Havana
 * @created 25/05/2024
 * @project basket
 */

public interface IBasketContentService {
    BasketContentDTO create(long basketId, long productId, int quantity) ;
    BasketContentDTO updateQuantity(long id, int quantity) ;
    void delete(long id) ;
    void deleteByBasketId(long basketId) ;
    List<BasketContent> getBasketContentsByBasketId(long basketId) ;
}
