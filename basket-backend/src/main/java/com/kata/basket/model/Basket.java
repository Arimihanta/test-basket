package com.kata.basket.model;

import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author Andriambolaharimihanta Havana
 * @created 25/05/2024
 * @project basket
 */

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Data
@Table(name = "baskets")
public class Basket {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToMany(mappedBy = "basket", cascade = CascadeType.ALL)
    private List<BasketContent> basketContentList ;

    @Column(name = "created_at", nullable = false)
    private LocalDateTime createdAt=LocalDateTime.now() ;

    @Column(name = "modified_at", nullable = false)
    private LocalDateTime modifiedAt=LocalDateTime.now() ;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;
}
