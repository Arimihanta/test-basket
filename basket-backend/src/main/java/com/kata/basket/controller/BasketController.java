package com.kata.basket.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kata.basket.dto.BasketContentDTO;
import com.kata.basket.dto.BasketDTO;
import com.kata.basket.dto.ProductDTO;
import com.kata.basket.dto.UserDTO;
import com.kata.basket.exception.UserNotFoundException;
import com.kata.basket.model.Basket;
import com.kata.basket.model.BasketContent;
import com.kata.basket.model.User;
import com.kata.basket.payload.BasketContentRequest;
import com.kata.basket.service.BasketContentServiceImpl;
import com.kata.basket.service.BasketServiceImpl;
import com.kata.basket.service.UserServiceImpl;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

/**
 * @author Andriambolaharimihanta Havana
 * @created 25/05/2024
 * @project basket
 */

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/baskets/")
public class BasketController {
    private final BasketServiceImpl basketService ;
    private final BasketContentServiceImpl basketContentService ;
    private final UserServiceImpl userService ;
    private final ModelMapper modelMapper;

    @GetMapping("list/{userId}")
    public ResponseEntity<?> getBasketsByUserId(@PathVariable Long userId) {
        List<BasketDTO> basketDTOS=basketService.getBasketsByUserId(userId) ;
        return ResponseEntity.ok(basketDTOS);
    }

    @PostMapping("create-or-get/{userId}")
    public ResponseEntity<?> createOrGet(@PathVariable Long userId) {
        BasketDTO basketDTO=basketService.createOrGetLastBasket(userId) ;
        return ResponseEntity.ok(basketDTO);
    }

    @PostMapping("create-or-update")
    public ResponseEntity<?> createOrUpdate(@RequestBody Map<String, Object> payload) {
        Basket basket=new Basket() ;
        try{
            long userId=Long.parseLong(payload.get("userId").toString()) ;
            BasketDTO basketDTO=basketService.createOrGetLastBasket(userId) ;
            basket=modelMapper.map(basketDTO, Basket.class) ;

            ObjectMapper mapper = new ObjectMapper();
            BasketContentRequest[] basketContentRequests =mapper.readValue(mapper.writeValueAsString(payload.get("contents")),BasketContentRequest[].class) ;
            basketContentService.deleteByBasketId(basketDTO.getId());
            for(BasketContentRequest basketContentRequest:basketContentRequests){
                basketContentService.create(
                        basket.getId(),
                        basketContentRequest.getProductId(),
                        basketContentRequest.getQuantity()) ;
            }
        }catch (JsonProcessingException e){
            e.printStackTrace();
        }

        return new ResponseEntity<>(basket,HttpStatus.OK);
    }
}
