package com.kata.basket.controller;

import com.kata.basket.dto.ProductDTO;
import com.kata.basket.exception.ProductNotFoundException;
import com.kata.basket.service.ProductServiceImpl;
import lombok.RequiredArgsConstructor ;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;

/**
 * @author Andriambolaharimihanta Havana
 * @created 25/05/2024
 * @project basket
 */

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/products/")
public class ProductController {
    private final ProductServiceImpl productService ;
    @GetMapping("list")
    public ResponseEntity<?> getAllProducts() {
        List<ProductDTO> products=productService.findAll() ;
        return ResponseEntity.ok(products);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getProductById(@PathVariable Long id) {
        ProductDTO product ;
        try {
            product= productService.findById(id) ;
        }
        catch (ProductNotFoundException e){
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(product);
    }
}
