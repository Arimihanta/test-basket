package com.kata.basket.dto;

import com.kata.basket.model.Basket;
import lombok.Data;

/**
 * @author Andriambolaharimihanta Havana
 * @created 25/05/2024
 * @project basket
 */

@Data
public class BasketContentDTO {
    private Long id ;
    private ProductDTO product ;
    private int quantity ;
}
