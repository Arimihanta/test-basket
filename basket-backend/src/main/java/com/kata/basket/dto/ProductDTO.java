package com.kata.basket.dto;

import jakarta.persistence.Column;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.Data;

/**
 * @author Andriambolaharimihanta Havana
 * @created 25/05/2024
 * @project basket
 */

@Data
public class ProductDTO {
    private Long id;
    private String name;
    private String description;
    private double price;
    private String image;
    private int stock;
}
