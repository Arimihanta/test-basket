package com.kata.basket.dto;

import jakarta.persistence.Column;
import lombok.Data;

/**
 * @author Andriambolaharimihanta Havana
 * @created 25/05/2024
 * @project basket
 */

@Data
public class UserDTO{
    private long id ;
    private String username ;
    private String firstName;
    private String lastName;
    private String phoneNumber ;
}
