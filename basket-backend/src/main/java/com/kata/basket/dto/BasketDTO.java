package com.kata.basket.dto;

import lombok.Data;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author Andriambolaharimihanta Havana
 * @created 25/05/2024
 * @project basket
 */

@Data
public class BasketDTO {
    private Long id;
    private List<BasketContentDTO> basketContentList;
    private LocalDateTime createdDate;
    private LocalDateTime modifiedDate;
    private UserDTO user;
}
