package com.kata.basket.exception;

/**
 * @author Andriambolaharimihanta Havana
 * @created 25/05/2024
 * @project basket
 */

public class ProductNotFoundException extends RuntimeException{
    public ProductNotFoundException(String message){super(message) ;}
}
