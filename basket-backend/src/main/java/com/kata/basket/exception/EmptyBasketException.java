package com.kata.basket.exception;

/**
 * @author Andriambolaharimihanta Havana
 * @created 25/05/2024
 * @project basket
 */

public class EmptyBasketException extends RuntimeException{
    public EmptyBasketException(String message){super(message) ;}
}
