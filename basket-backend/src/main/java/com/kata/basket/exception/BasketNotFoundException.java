package com.kata.basket.exception;

/**
 * @author Andriambolaharimihanta Havana
 * @created 25/05/2024
 * @project basket
 */

public class BasketNotFoundException extends RuntimeException{
    public BasketNotFoundException(String message){super(message) ;}
}
