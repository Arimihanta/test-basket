package com.kata.basket.exception;

/**
 * @author Andriambolaharimihanta Havana
 * @created 25/05/2024
 * @project basket
 */

public class UserNotFoundException extends RuntimeException{
    public UserNotFoundException(String message){super(message) ;}
}
