package com.kata.basket.exception;

/**
 * @author Andriambolaharimihanta Havana
 * @created 25/05/2024
 * @project basket
 */

public class NoStockLeftException extends RuntimeException{
    public NoStockLeftException(String message){super(message) ;}
}
