package com.kata.basket.exception;

/**
 * @author Andriambolaharimihanta Havana
 * @created 25/05/2024
 * @project basket
 */

public class BasketContentNotFoundException extends RuntimeException{
    public BasketContentNotFoundException(String message){super(message) ;}
}
