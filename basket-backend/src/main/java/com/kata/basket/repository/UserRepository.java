package com.kata.basket.repository;

import com.kata.basket.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Andriambolaharimihanta Havana
 * @created 25/05/2024
 * @project basket
 */

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
}