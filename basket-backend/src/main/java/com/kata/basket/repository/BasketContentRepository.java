package com.kata.basket.repository;

import com.kata.basket.model.BasketContent;
import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author Andriambolaharimihanta Havana
 * @created 25/05/2024
 * @project basket
 */

@Repository
public interface BasketContentRepository extends JpaRepository<BasketContent, Long> {
    @Modifying
    @Transactional
    @Query("delete from BasketContent bc where bc.basket.id = ?1")
    void deleteByBasketId(long basketId) ;

    @Query("select bc from BasketContent bc where bc.basket.id = ?1")
    List<BasketContent> getBasketContentsByBasketId(long basketId) ;
}