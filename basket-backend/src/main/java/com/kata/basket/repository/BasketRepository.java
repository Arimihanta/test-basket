package com.kata.basket.repository;

import com.kata.basket.model.Basket;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * @author Andriambolaharimihanta Havana
 * @created 25/05/2024
 * @project basket
 */

@Repository
public interface BasketRepository extends JpaRepository<Basket, Long> {
    @Query("select b from Basket b where b.user.id = ?1")
    List<Basket> getBasketsByUserId(long userId) ;
}
