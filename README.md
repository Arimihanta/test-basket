# test-basket

## Commencer

Ces instructions vous permettront d'obtenir une copie du projet opérationnel en local à des fins de développement et de test.

## Fonctionnalités

> - Afficher la liste des produits disponibles(avec stock disponible, prix...).
> - Ajouter, modifier ou retirer des produits dans un panier.
> - Retirez des prodtuits du panier si la date dépasse du 24h

# Prérequis

- Node.js
- java 21
- Emulateur mobile

## Configuration

Télécharger et extraire les sources dans l'emplacement de votre choix.

### Front-end

Dans le dossier racine de l'application mobile`test-basket\basket-front`, il y a un fichier `env.ts`. Veuillez modifier l'adresse IP dans `API_BASE_URL` par votre adresse IP.

## Build

### Back-end

```
cd basket-backend
./gradlew bootRun
```

### Front-end

`cd basket-front`

`npm install` ou `yarn`

`npm run android` ou `yarn android`(si vous utilisez yarn), remplacez android par `ios` ou `web` selon votre appareil.

## Auteur

**Havana Andriambolaharimihanta** - [aarimihanta@gmail.com](mailto:aarimihanta@gmail.com)
