import { Fonts } from "@/constants/Fonts";
import { StyleSheet } from "react-native";

export const globalStyle = StyleSheet.create({
  container: {
    flex: 1,
    padding: 0,
  },
  headerTitle: {
    fontWeight: "bold",
    fontSize: 15,
    color: "white",
    fontFamily: Fonts["Nunito Sans"][400],
  },
  listItemsWrapper: {
    flexDirection: "column",
    gap: 10,
    margin: 10,
  },
});
