export const Fonts = {
  "Nunito Sans": {
    400: "NunitoSans-Regular",
    500: "NunitoSans-Medium",
    600: "NunitoSans-SemiBold",
    700: "NunitoSans-Bold",
    800: "NunitoSans-ExtraBold",
    900: "NunitoSans-Black",
  },
};
