import React from "react";
import { StyleSheet } from "react-native";
import { Link } from "expo-router";
import { Fonts } from "@/constants/Fonts";
import { MaterialCommunityIcons } from "@expo/vector-icons";

export const BasketActionButton = () => {
  return (
    <Link href="basket" style={localStyles.basketActionButton}>
      <MaterialCommunityIcons name="cart-arrow-down" size={28} color="white" />
    </Link>
  );
};

const localStyles = StyleSheet.create({
  basketActionButton: {
    fontWeight: "bold",
    fontSize: 14,
    color: "white",
    fontFamily: Fonts["Nunito Sans"][400],
  },
});
