import React from "react";
import { Fonts } from "@/constants/Fonts";
import { IBasketContent, IBasketContentRequest, IProduct } from "@/types";
import { StyleSheet, View, Text, Image, Pressable } from "react-native";
import { Colors } from "@/constants/Colors";
import defaultImage from "../../assets/images/icon.png";
import { MaterialIcons, MaterialCommunityIcons } from "@expo/vector-icons";

type BasketContentItemProps = {
  item: IBasketContent;
  checkedBasketContent?: IBasketContentRequest;
  onPressCart: (type: string, productId: number) => void;
  onRemoveContentBasket: (productId: number) => void;
};

export const BasketContentItem = ({
  item,
  checkedBasketContent,
  onPressCart,
  onRemoveContentBasket,
}: BasketContentItemProps) => {
  return (
    <View style={localStyle.productItem}>
      <Image
        source={item.product.image ? item.product.image : defaultImage}
        style={localStyle.image}
      />
      <View style={localStyle.containerInfo}>
        <Text style={localStyle.name}>{item.product.name}</Text>
        <View style={localStyle.amountWrapper}>
          <View style={localStyle.amountContentWrapper}>
            <Text>PU</Text>
            <Text style={localStyle.price}>
              {item.product.price.toLocaleString("fr-FR", {
                style: "currency",
                currency: "EUR",
              })}
            </Text>
          </View>
          <View style={localStyle.amountContentWrapper}>
            <Text>PT</Text>
            <Text style={localStyle.price}>
              {(
                item.product.price * checkedBasketContent?.quantity
              ).toLocaleString("fr-FR", {
                style: "currency",
                currency: "EUR",
              })}
            </Text>
          </View>
        </View>
        <Text
          style={localStyle.description}
          numberOfLines={2}
          ellipsizeMode="tail"
        >
          {item.product.description}
        </Text>
        <View style={localStyle.availableStockWrapper}>
          {item.product.stock > 0 ? (
            <>
              <Text style={localStyle.labelAvailableStock}>
                Stock disponible
              </Text>
              <Text style={localStyle.valueAvailableStock}>
                {item.product.stock}
              </Text>
            </>
          ) : (
            <Text style={localStyle.outOfStockText}>Stock epuisé</Text>
          )}
        </View>
        {item.product.stock > 0 && (
          <View style={localStyle.actionButtonWrapper}>
            <Pressable
              onPress={() => onPressCart("minus", item.product.id)}
              style={({ pressed }) => [
                {
                  backgroundColor: pressed
                    ? "#61af0010"
                    : Colors.light.quaternary,
                },
              ]}
              disabled={
                !checkedBasketContent ||
                checkedBasketContent.quantity < 1 ||
                item.product.stock < 1
              }
            >
              <MaterialCommunityIcons name="minus" size={24} color="black" />
            </Pressable>
            <Text>
              {checkedBasketContent ? checkedBasketContent.quantity : 0}
            </Text>
            <Pressable
              onPress={() => onPressCart("add", item.product.id)}
              style={({ pressed }) => [
                {
                  backgroundColor: pressed
                    ? "#61af0010"
                    : Colors.light.quaternary,
                },
              ]}
              disabled={
                (checkedBasketContent &&
                  checkedBasketContent.quantity > item.product.stock) ||
                item.product.stock < 1
              }
            >
              <MaterialIcons name="add" size={24} color="black" />
            </Pressable>
          </View>
        )}
      </View>
      <View style={localStyle.actionButtonWrapper}>
        <Pressable
          onPress={() => onRemoveContentBasket(item.product.id)}
          style={({ pressed }) => [
            localStyle.actionButton,
            {
              backgroundColor: pressed ? "#61af0010" : Colors.light.danger,
            },
          ]}
        >
          <MaterialIcons name="delete" size={24} color="white" />
        </Pressable>
      </View>
    </View>
  );
};
const localStyle = StyleSheet.create({
  productItem: {
    flex: 1,
    borderBottomWidth: 0.75,
    borderColor: "#E1E3E8",
    flexDirection: "row",
    alignItems: "center",
    borderWidth: 1,
    borderRadius: 8,
    padding: 10,
  },
  image: {
    width: 80,
    height: 80,
  },
  containerInfo: {
    flexDirection: "column",
    gap: 7,
    marginStart: 10,
    flex: 1,
  },
  name: {
    fontFamily: Fonts["Nunito Sans"][400],
    fontSize: 14,
    color: "#000",
    flexWrap: "wrap",
  },
  price: {
    fontFamily: Fonts["Nunito Sans"][600],
    fontSize: 14,
    backgroundColor: Colors.light.secondary,
    color: "#fff",
    paddingVertical: 3,
    paddingHorizontal: 7,
    borderRadius: 5,
  },
  description: {
    fontFamily: Fonts["Nunito Sans"][400],
    fontSize: 12,
    color: "#777",
    flexWrap: "wrap",
  },
  availableStockWrapper: {
    flexDirection: "row",
    alignItems: "center",
    gap: 7,
  },
  labelAvailableStock: {
    fontFamily: Fonts["Nunito Sans"][400],
    fontSize: 12,
    color: "#707070",
  },
  valueAvailableStock: {
    fontFamily: Fonts["Nunito Sans"][500],
    fontSize: 14,
    color: "#000",
  },
  outOfStockText: {
    fontFamily: Fonts["Nunito Sans"][400],
    fontSize: 12,
    color: "#fff",
    backgroundColor: Colors.light.danger,
    paddingVertical: 3,
    paddingHorizontal: 7,
    borderRadius: 5,
  },
  actionButtonWrapper: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    gap: 7,
  },
  actionButton: {
    padding: 5,
    borderRadius: 25,
  },
  amountWrapper: {
    flexDirection: "row",
    gap: 15,
  },
  amountContentWrapper: {
    flexDirection: "row",
    gap: 7,
  },
});
