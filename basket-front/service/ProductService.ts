import { IProduct, ApiResponse } from "@/types";
import axios from "./axios";

export const getAllProducts = async (): Promise<ApiResponse<IProduct[]>> => {
  const response: ApiResponse<IProduct[]> = await axios.get("products/list");
  return {
    status: response.status,
    data: response.data,
  };
};
