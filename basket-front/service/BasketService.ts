import { IBasket, ApiResponse, IBasketRequest } from "@/types";
import axios from "./axios";

export const getBasketByUserId = async (
  userId: number
): Promise<ApiResponse<IBasket>> => {
  const response: ApiResponse<IBasket> = await axios.post(
    `baskets/create-or-get/${userId}`
  );
  return {
    status: response.status,
    data: response.data,
  };
};

export const createOrUpdateBasket = async (
  basket: IBasketRequest
): Promise<ApiResponse<IBasket>> => {
  const response: ApiResponse<IBasket> = await axios.post(
    `baskets/create-or-update`,
    basket
  );
  return {
    status: response.status,
    data: response.data,
  };
};
