import React, { useState, useEffect } from "react";
import { Fonts } from "@/constants/Fonts";
import {
  Text,
  View,
  StyleSheet,
  SafeAreaView,
  ScrollView,
  ActivityIndicator,
} from "react-native";
import { Link } from "expo-router";
import { globalStyle } from "@/styles/Styles";
import {
  ApiResponse,
  IBasketContentRequest,
  IBasket,
  IBasketContent,
} from "@/types";
import { BasketContentItem } from "@/components/basket/BasketContentItem";
import {
  createOrUpdateBasket,
  getBasketByUserId,
} from "@/service/BasketService";

const BasketContentList = () => {
  const [loading, setLoading] = useState<boolean>(false);
  const [selectedBasketContents, setSelectedBasketContents] = useState<
    IBasketContentRequest[]
  >([]);
  const [basket, setBasket] = useState<IBasket>();

  const fetchAllData = async () => {
    setLoading(true);
    await fetchBasket();
    setLoading(false);
  };

  const fetchBasket = async () => {
    const response: ApiResponse<IBasket> = await getBasketByUserId(1);
    if (response.status === 200) {
      const basket = response.data;
      if (basket.basketContentList && basket.basketContentList.length) {
        const newSelectedBasketContents = basket.basketContentList.map((x) => ({
          productId: x.product.id,
          quantity: x.quantity,
        }));
        setSelectedBasketContents(newSelectedBasketContents);
      }
      setBasket(basket);
    }
  };

  const selectProduct = async (type: string, productId: number) => {
    const index = selectedBasketContents.findIndex(
      (x) => x.productId === productId
    );
    if (index !== -1) {
      const updatedSelectedBasketContents = [...selectedBasketContents];
      const currentBasketContent = updatedSelectedBasketContents[index];
      type === "add"
        ? currentBasketContent.quantity++
        : currentBasketContent.quantity--;
      updatedSelectedBasketContents[index] = currentBasketContent;
      await handleAddOrUpdateBasket(updatedSelectedBasketContents);
    } else {
      if (type === "add") {
        const basketContent = {
          productId,
          quantity: 1,
        };
        setSelectedBasketContents([...selectedBasketContents, basketContent]);
      }
    }
  };

  const handleAddOrUpdateBasket = async (baskets: IBasketContentRequest[]) => {
    const basketContent = baskets.filter((x) => x.quantity !== 0);
    const response = await createOrUpdateBasket({
      userId: 1,
      contents: basketContent,
    });
    if (response.status === 200) {
      await fetchAllData();
    }
  };

  const handleRemoveContentBasket = async (productId: number) => {
    const newSelectedBasketContents = selectedBasketContents.filter(
      (x) => x.productId !== productId
    );
    await handleAddOrUpdateBasket(newSelectedBasketContents);
  };

  useEffect(() => {
    fetchAllData();
  }, []);

  return (
    <SafeAreaView style={globalStyle.container}>
      {loading ? (
        <View style={localStyle.centered}>
          <ActivityIndicator size="large" color="#0000ff" />
        </View>
      ) : basket?.basketContentList?.length ? (
        <ScrollView>
          <View style={globalStyle.listItemsWrapper}>
            {basket?.basketContentList
              .filter((x) =>
                selectedBasketContents.some((y) => x.product.id === y.productId)
              )
              .map((item: IBasketContent, key) => (
                <BasketContentItem
                  key={key}
                  item={item}
                  checkedBasketContent={selectedBasketContents.find(
                    (x) => x.productId === item.product.id
                  )}
                  onPressCart={selectProduct}
                  onRemoveContentBasket={handleRemoveContentBasket}
                />
              ))}
          </View>
        </ScrollView>
      ) : (
        <View style={localStyle.centered}>
          <Text>Le panier est vide.</Text>
        </View>
      )}
    </SafeAreaView>
  );
};

const localStyle = StyleSheet.create({
  centered: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
});

export default BasketContentList;
