import React, { useState, useEffect, useCallback } from "react";
import { Fonts } from "@/constants/Fonts";
import {
  Text,
  View,
  StyleSheet,
  SafeAreaView,
  ScrollView,
  ActivityIndicator,
  Pressable,
} from "react-native";
import { globalStyle } from "@/styles/Styles";
import { getAllProducts } from "@/service/ProductService";
import { ApiResponse, IProduct, IBasketContentRequest, IBasket } from "@/types";
import { ProductItem } from "@/components/product/ProductItem";
import {
  createOrUpdateBasket,
  getBasketByUserId,
} from "@/service/BasketService";
import { Colors } from "@/constants/Colors";
import { useFocusEffect } from "@react-navigation/native";

const ProductsList = () => {
  const [products, setProducts] = useState<IProduct[]>([]);
  const [loading, setLoading] = useState<boolean>(false);
  const [selectedBasketContents, setSelectedBasketContents] = useState<
    IBasketContentRequest[]
  >([]);
  const [basket, setBasket] = useState<IBasket>();
  const [showBasketButton, setShowBasketButton] = useState<boolean>(false);

  const fetchAllData = async () => {
    setLoading(true);
    await fetchAllProducts();
    await fetchBasket();
    setLoading(false);
  };

  const fetchAllProducts = async () => {
    try {
      const response: ApiResponse<IProduct[]> = await getAllProducts();

      if (response.status === 200) {
        setProducts(response.data);
      } else {
        console.error("Failed to fetch products");
      }
    } catch (err) {
      console.error("Error fetching products: ", err);
    }
  };

  const fetchBasket = async () => {
    const response: ApiResponse<IBasket> = await getBasketByUserId(1);
    if (response.status === 200) {
      const basket = response.data;
      if (basket.basketContentList && basket.basketContentList.length) {
        const selectedBasketContents = basket.basketContentList.map((x) => ({
          productId: x.product.id,
          quantity: x.quantity,
        }));
        setSelectedBasketContents(selectedBasketContents);
      }
      setBasket(basket);
    }
  };

  const selectProduct = (type: string, productId: number): void => {
    const index = selectedBasketContents.findIndex(
      (x) => x.productId === productId
    );
    setShowBasketButton(true);
    if (index !== -1) {
      const updatedSelectedBasketContents = [...selectedBasketContents];
      const currentBasketContent = updatedSelectedBasketContents[index];
      type === "add"
        ? currentBasketContent.quantity++
        : currentBasketContent.quantity--;
      updatedSelectedBasketContents[index] = currentBasketContent;
      setSelectedBasketContents(updatedSelectedBasketContents);
    } else {
      if (type === "add") {
        const basketContent = {
          productId,
          quantity: 1,
        };
        setSelectedBasketContents([...selectedBasketContents, basketContent]);
      }
    }
  };

  const handleAddBasket = async () => {
    const basketContent = selectedBasketContents.filter(
      (x) => x.quantity !== 0
    );
    const response = await createOrUpdateBasket({
      userId: 1,
      contents: basketContent,
    });
    if (response.status === 200) {
      await fetchAllData();
      setShowBasketButton(false);
    }
  };

  useFocusEffect(
    useCallback(() => {
      fetchAllData();
    }, [])
  );

  useEffect(() => {
    fetchAllData();
  }, []);

  useEffect(() => {
    fetchBasket();
  }, [products]);

  return (
    <SafeAreaView style={globalStyle.container}>
      {loading ? (
        <View style={localStyle.centered}>
          <ActivityIndicator size="large" color="#0000ff" />
        </View>
      ) : (
        <ScrollView>
          <View style={globalStyle.listItemsWrapper}>
            {products.map((product: IProduct, key) => (
              <ProductItem
                key={key}
                product={product}
                basketContent={selectedBasketContents.find(
                  (x) => x.productId === product.id
                )}
                onPressCart={selectProduct}
              />
            ))}
          </View>
        </ScrollView>
      )}
      {showBasketButton && (
        <Pressable style={localStyle.buttonCart} onPress={handleAddBasket}>
          <Text style={localStyle.buttonCartText}>{`${
            basket?.basketContentList?.length
              ? "Modifier panier"
              : "Ajouter dans panier"
          }`}</Text>
        </Pressable>
      )}
    </SafeAreaView>
  );
};

const localStyle = StyleSheet.create({
  centered: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  buttonCart: {
    alignItems: "center",
    justifyContent: "center",
    paddingVertical: 7,
    paddingHorizontal: 24,
    borderRadius: 25,
    elevation: 7,
    backgroundColor: Colors.light.primary,
    position: "absolute",
    right: 20,
    bottom: 20,
  },
  buttonCartText: {
    fontFamily: Fonts["Nunito Sans"][600],
    fontSize: 14,
    color: "#fff",
  },
});

export default ProductsList;
