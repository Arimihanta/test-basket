import {
  DarkTheme,
  DefaultTheme,
  ThemeProvider,
} from "@react-navigation/native";
import { useFonts } from "expo-font";
import { Stack } from "expo-router";
import * as SplashScreen from "expo-splash-screen";
import { useEffect } from "react";
import "react-native-reanimated";
import { useColorScheme } from "@/hooks/useColorScheme";
import { Colors } from "@/constants/Colors";
import { globalStyle } from "@/styles/Styles";
import { BasketActionButton } from "@/components/basket/BasketActionButton";

// Prevent the splash screen from auto-hiding before asset loading is complete.
SplashScreen.preventAutoHideAsync();

export default function RootLayout() {
  const colorScheme = useColorScheme();
  const [loaded] = useFonts({
    "NunitoSans-Regular": require("../assets/fonts/nunito_sans_10pt_regular.ttf"),
    "NunitoSans-Medium": require("../assets/fonts/nunito_sans_10pt_medium.ttf"),
    "NunitoSans-SemiBold": require("../assets/fonts/nunito_sans_10pt_semi_bold.ttf"),
    "NunitoSans-Bold": require("../assets/fonts/nunito_sans_10pt_bold.ttf"),
    "NunitoSans-ExtraBold": require("../assets/fonts/nunito_sans_10pt_extra_bold.ttf"),
    "NunitoSans-Black": require("../assets/fonts/nunito_sans_10pt_black.ttf"),
  });

  useEffect(() => {
    if (loaded) {
      SplashScreen.hideAsync();
    }
  }, [loaded]);

  if (!loaded) {
    return null;
  }

  return (
    <ThemeProvider value={colorScheme === "dark" ? DarkTheme : DefaultTheme}>
      <Stack
        screenOptions={{
          headerStyle: {
            backgroundColor: Colors.light.primary,
          },
          headerTintColor: "#fff",
          headerTitleStyle: globalStyle.headerTitle,
        }}
      >
        <Stack.Screen
          name="index"
          options={{
            headerShown: true,
            headerTitle: "Liste des produits",
            headerRight: (props) => <BasketActionButton {...props} />,
          }}
        />
        <Stack.Screen
          name="basket"
          options={{
            headerShown: true,
            headerTitle: "Mon Panier",
          }}
        />
        <Stack.Screen name="+not-found" />
      </Stack>
    </ThemeProvider>
  );
}
