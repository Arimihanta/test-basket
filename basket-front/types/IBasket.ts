import { IBasketContent } from "./IBasketContent";
import { IUser } from "./IUser";

export interface IBasket {
  id: number;
  basketContentList?: IBasketContent[];
  quantity: number;
  createdDate: Date;
  modifiedDate: Date;
  user: IUser;
}
