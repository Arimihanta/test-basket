export interface IBasketContentRequest {
  productId: number;
  quantity: number;
}
