import { IBasketContentRequest } from "./IBasketContentRequest";

export interface IBasketRequest {
  userId: number;
  contents: IBasketContentRequest[];
}
