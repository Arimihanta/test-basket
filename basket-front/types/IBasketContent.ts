import { IProduct } from "./IProduct";

export interface IBasketContent {
  id: number;
  product: IProduct;
  quantity: number;
}
